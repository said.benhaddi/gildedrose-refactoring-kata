﻿using NUnit.Framework;
using System.Collections.Generic;

namespace csharp.Tests {
    [TestFixture]
    public class GildedRoseTest {
        [Test]
        public void foo() {
            IList<Item> Items = new List<Item> { new Item { Name = "foo", SellIn = 0, Quality = 0 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual("foo", Items[0].Name);
        }

        #region Standard Item
        [Test]
        public void UpdateQualityOfStandardItemWithQualityOfExaclty0AndPassedSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "+5 Dexterity Vest", SellIn = 0, Quality = 0 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(0, Items[0].Quality);
        }

        [Test]
        public void UpdateQualityOfStandardItemWithQualityOfExaclty0AndNotPassedSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "+5 Dexterity Vest", SellIn = 1, Quality = 0 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(0, Items[0].Quality);
        }

        [Test]
        public void UpdateQualityOfStandardItemWithQualityOfExaclty0AndNegativeSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "+5 Dexterity Vest", SellIn = -1, Quality = 0 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(-2, Items[0].SellIn);
        }

        [Test]
        public void UpdateQualityOfStandardItemWithQualityOfExaclty0AndPositiveSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "+5 Dexterity Vest", SellIn = 2, Quality = 0 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(1, Items[0].SellIn);
        }

        [Test]
        public void UpdateQualityOfStandardItemWithQualityOfExaclty50AndPassedSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "+5 Dexterity Vest", SellIn = 0, Quality = 50 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(48, Items[0].Quality);
        }

        [Test]
        public void UpdateQualityOfStandardItemWithQualityOfExaclty50AndNotPassedSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "+5 Dexterity Vest", SellIn = 1, Quality = 50 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(49, Items[0].Quality);
        }

        [Test]
        public void UpdateQualityOfStandardItemWithQualityBetween0And50AndPassedSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "+5 Dexterity Vest", SellIn = 0, Quality = 36 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(34, Items[0].Quality);
        }

        [Test]
        public void UpdateQualityOfStandardItemWithQualityBetween0And50AndNotPassedSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "+5 Dexterity Vest", SellIn = 1, Quality = 36 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(35, Items[0].Quality);
        }
        #endregion

        #region Aged Bree
        [Test]
        public void UpdateQualityOfAgedBreeItemWithQualityOfExaclty0AndPassedSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Aged Brie", SellIn = 0, Quality = 0 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(2, Items[0].Quality);
        }

        [Test]
        public void UpdateQualityOfAgedBreeItemWithQualityOfExaclty0AndNotPassedSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Aged Brie", SellIn = 1, Quality = 0 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(1, Items[0].Quality);
        }

        [Test]
        public void UpdateQualityOfAgedBreeItemWithQualityOfExaclty0AndNegativeSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Aged Brie", SellIn = -1, Quality = 0 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(-2, Items[0].SellIn);
        }

        [Test]
        public void UpdateQualityOfAgedBreeItemWithQualityOfExaclty0AndPositiveSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Aged Brie", SellIn = 5, Quality = 0 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(4, Items[0].SellIn);
        }

        [Test]
        public void UpdateQualityOfAgedBreeItemWithQualityOfExaclty50AndPassedSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Aged Brie", SellIn = 0, Quality = 50 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(50, Items[0].Quality);
        }

        [Test]
        public void UpdateQualityOfAgedBreeItemWithQualityOfExaclty50AndNotPassedSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Aged Brie", SellIn = 1, Quality = 50 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(50, Items[0].Quality);
        }

        [Test]
        public void UpdateQualityOfAgedBreeItemWithQualityBetween0And50AndPassedSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Aged Brie", SellIn = 0, Quality = 36 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(38, Items[0].Quality);
        }

        [Test]
        public void UpdateQualityOfAgedBreeItemWithQualityBetween0And50AndNotPassedSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Aged Brie", SellIn = 1, Quality = 36 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(37, Items[0].Quality);
        }
        #endregion

        #region Sulfuras, Hand of Ragnaros
        [Test]
        public void UpdateQualityOfSulfurasItemWithQualityOfExaclty0AndPassedSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Sulfuras, Hand of Ragnaros", SellIn = 0, Quality = 0 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(0, Items[0].Quality);
        }

        [Test]
        public void UpdateQualityOfSulfurasItemWithQualityOfExaclty0AndNotPassedSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Sulfuras, Hand of Ragnaros", SellIn = 1, Quality = 0 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(0, Items[0].Quality);
        }

        [Test]
        public void UpdateQualityOfSulfurasItemWithQualityOfExaclty0AndNegativeSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Sulfuras, Hand of Ragnaros", SellIn = -1, Quality = 0 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(-1, Items[0].SellIn);
        }

        [Test]
        public void UpdateQualityOfSulfurasItemWithQualityOfExaclty0AndPositiveSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Sulfuras, Hand of Ragnaros", SellIn = 5, Quality = 0 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(5, Items[0].SellIn);
        }

        [Test]
        public void UpdateQualityOfSulfurasItemWithQualityOfExaclty50AndPassedSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Sulfuras, Hand of Ragnaros", SellIn = 0, Quality = 50 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(50, Items[0].Quality);
        }

        [Test]
        public void UpdateQualityOfSulfurasItemWithQualityOfExaclty50AndNotPassedSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Sulfuras, Hand of Ragnaros", SellIn = 1, Quality = 50 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(50, Items[0].Quality);
        }

        [Test]
        public void UpdateQualityOfSulfurasItemWithQualityBetween0And50AndPassedSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Sulfuras, Hand of Ragnaros", SellIn = 0, Quality = 36 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(36, Items[0].Quality);
        }

        [Test]
        public void UpdateQualityOfSulfurasItemWithQualityBetween0And50AndNotPassedSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Sulfuras, Hand of Ragnaros", SellIn = 1, Quality = 36 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(36, Items[0].Quality);
        }
        #endregion

        #region Backstage passes to a TAFKAL80ETC concert
        [Test]
        public void UpdateQualityOfBackstagePassItemWithQualityOfExaclty0AndPassedSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 0, Quality = 0 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(0, Items[0].Quality);
        }

        [Test]
        public void UpdateQualityOfBackstagePassItemWithQualityOfExaclty0AndNotPassedSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 1, Quality = 0 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(3, Items[0].Quality);
        }

        [Test]
        public void UpdateQualityOfBackstagePassItemWithQualityOfExaclty0AndNegativeSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = -1, Quality = 0 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(-2, Items[0].SellIn);
        }

        [Test]
        public void UpdateQualityOfBackstagePassItemWithQualityOfExaclty0AndPositiveSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 5, Quality = 0 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(4, Items[0].SellIn);
        }

        [Test]
        public void UpdateQualityOfBackstagePassItemWithQualityOfExaclty50AndPassedSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 0, Quality = 50 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(0, Items[0].Quality);
        }

        [Test]
        public void UpdateQualityOfBackstagePassItemWithQualityOfExaclty50AndNotPassedSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 1, Quality = 50 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(50, Items[0].Quality);
        }

        [Test]
        public void UpdateQualityOfBackstagePassItemWithQualityBetween0And50AndPassedSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 0, Quality = 36 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(0, Items[0].Quality);
        }

        [Test]
        public void UpdateQualityOfBackstagePassItemWithQualityBetween0And50AndNotPassedSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 1, Quality = 36 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(39, Items[0].Quality);
        }

        [Test]
        public void UpdateQualityOfBackstagePassItemWithQualityBetween0And50AndNotSellInBetween5And10() {
            IList<Item> Items = new List<Item> { new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 6, Quality = 36 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(38, Items[0].Quality);
        }

        [Test]
        public void UpdateQualityOfBackstagePassItemWithQualityBetween0And50AndNotSellInBetween1And5() {
            IList<Item> Items = new List<Item> { new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 3, Quality = 36 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(39, Items[0].Quality);
        }
        #endregion

        #region Conjured Mana Cake
        [Test]
        public void UpdateQualityOfConjuredItemWithQualityOfExaclty0AndPassedSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Conjured Mana Cake", SellIn = 0, Quality = 0 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(0, Items[0].Quality);
        }

        [Test]
        public void UpdateQualityOfConjuredItemWithQualityOfExaclty0AndNotPassedSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Conjured Mana Cake", SellIn = 1, Quality = 0 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(0, Items[0].Quality);
        }

        [Test]
        public void UpdateQualityOfConjuredItemWithQualityOfExaclty0AndNegativeSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Conjured Mana Cake", SellIn = -1, Quality = 0 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(-2, Items[0].SellIn);
        }

        [Test]
        public void UpdateQualityOfConjuredItemWithQualityOfExaclty0AndPositiveSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Conjured Mana Cake", SellIn = 5, Quality = 0 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(4, Items[0].SellIn);
        }

        [Test]
        public void UpdateQualityOfConjuredItemWithQualityOfExaclty50AndPassedSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Conjured Mana Cake", SellIn = 0, Quality = 50 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(46, Items[0].Quality);
        }

        [Test]
        public void UpdateQualityOfConjuredItemWithQualityOfExaclty50AndNotPassedSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Conjured Mana Cake", SellIn = 1, Quality = 50 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(48, Items[0].Quality);
        }

        [Test]
        public void UpdateQualityOfConjuredItemWithQualityBetween0And50AndPassedSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Conjured Mana Cake", SellIn = 0, Quality = 36 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(32, Items[0].Quality);
        }

        [Test]
        public void UpdateQualityOfConjuredItemWithQualityBetween0And50AndNotPassedSellingDate() {
            IList<Item> Items = new List<Item> { new Item { Name = "Conjured Mana Cake", SellIn = 1, Quality = 36 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(34, Items[0].Quality);
        }

        [Test]
        public void UpdateQualityOfConjuredItemWithQualityBetween0And50AndNotSellInBetween5And10() {
            IList<Item> Items = new List<Item> { new Item { Name = "Conjured Mana Cake", SellIn = 6, Quality = 36 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(34, Items[0].Quality);
        }

        [Test]
        public void UpdateQualityOfConjuredItemWithQualityBetween0And50AndNotSellInBetween1And5() {
            IList<Item> Items = new List<Item> { new Item { Name = "Conjured Mana Cake", SellIn = 3, Quality = 36 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(34, Items[0].Quality);
        }
        #endregion
    }
}
