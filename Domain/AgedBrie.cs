﻿namespace csharp.Domain {
    public class AgedBrie : ItemWrapper {
        public AgedBrie(Item item) : base(item) {
        }

        public override ItemWrapper UpdateQuality() {
            IncrementQuality();
            DecrementSellIn();
            if (SellDateIsPassed) {
                IncrementQuality();
            }
            return this;
        }
    }
}
