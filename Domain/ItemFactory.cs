﻿using System;

namespace csharp.Domain {
    public static class ItemFactory {
        public static ItemWrapper CreateInstance(Item item) {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            switch (item.Name) {
                case "Aged Brie":
                    return new AgedBrie(item);
                case "Backstage passes to a TAFKAL80ETC concert":
                    return new BackstagePass(item);
                case "Sulfuras, Hand of Ragnaros":
                    return new Sulfuras(item);
                case "Conjured Mana Cake":
                    return new Conjured(item);
                default:
                    return new StandardItem(item);
            }
        }
    }
}
