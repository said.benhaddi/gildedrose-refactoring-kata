﻿namespace csharp.Domain {
    public class Sulfuras : ItemWrapper {
        public Sulfuras(Item item) : base(item) {
        }

        public override ItemWrapper UpdateQuality() {
            return this;
        }
    }
}
