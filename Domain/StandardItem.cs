﻿namespace csharp.Domain {
    public class StandardItem : ItemWrapper {
        public StandardItem(Item item) : base(item) {
        }

        public override ItemWrapper UpdateQuality() {
            DecrementQuality();
            DecrementSellIn();
            if (SellDateIsPassed) {
                DecrementQuality();
            }
            return this;
        }
    }
}
