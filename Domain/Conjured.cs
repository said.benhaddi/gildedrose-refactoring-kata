﻿namespace csharp.Domain {
    public class Conjured : ItemWrapper {
        public Conjured(Item item) : base(item) {
        }

        public override ItemWrapper UpdateQuality() {
            DecrementQuality();
            DecrementQuality();
            DecrementSellIn();
            if (SellDateIsPassed) {
                DecrementQuality();
                DecrementQuality();
            }
            return this;
        }
    }
}
