﻿using System;

namespace csharp.Domain {
    public abstract class ItemWrapper {
        protected const int MAX_VALUE_FOR_QUALITY = 50;
        protected const int MIN_VALUE_FOR_QUALITY = 0;
        protected const int TEN_DAYS_LIMIT_FOR_SELL = 11;
        protected const int FIVE_DAYS_LIMIT_FOR_SELL = 6;
        protected readonly Item _item;

        public ItemWrapper(Item item) {
            _item = item ?? throw new ArgumentNullException(nameof(item));
        }

        public abstract ItemWrapper UpdateQuality();

        protected string Name {
            get { return _item.Name; }
            set { _item.Name = value; }
        }

        protected int SellIn {
            get { return _item.SellIn; }
            set { _item.SellIn = value; }
        }

        protected int Quality {
            get { return _item.Quality; }
            set { _item.Quality = value; }
        }

        protected bool StillTenDaysOrLessToSellTheItem =>
            SellIn < TEN_DAYS_LIMIT_FOR_SELL;

        protected bool StillFiveDaysOrLessToSellTheItem =>
            SellIn < FIVE_DAYS_LIMIT_FOR_SELL;

        protected bool MaxValueForQuantityReached =>
            Quality >= MAX_VALUE_FOR_QUALITY;

        protected bool MinValueForQuantityReached =>
            Quality <= MIN_VALUE_FOR_QUALITY;

        protected bool SellDateIsPassed =>
            SellIn < 0;

        protected void DecrementQuality() {
            if (!MinValueForQuantityReached) {
                Quality -= 1;
            }
        }

        protected void IncrementQuality() {
            if (!MaxValueForQuantityReached) {
                Quality += 1;
            }
        }

        protected void DecrementSellIn() {
            SellIn -= 1;
        }
    }
}
