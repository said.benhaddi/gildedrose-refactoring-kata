﻿namespace csharp.Domain {
    public class BackstagePass : ItemWrapper {
        public BackstagePass(Item item) : base(item) {
        }

        public override ItemWrapper UpdateQuality() {
            IncrementQuality();
            if (!MaxValueForQuantityReached) {
                if (StillTenDaysOrLessToSellTheItem) {
                    IncrementQuality();
                }
                if (StillFiveDaysOrLessToSellTheItem) {
                    IncrementQuality();
                }
            }
            DecrementSellIn();
            if (SellDateIsPassed) {
                Quality = MIN_VALUE_FOR_QUALITY;
            }
            return this;
        }
    }
}
